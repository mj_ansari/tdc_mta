
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import random
import os
import ast
import timeit
import time
from functools import reduce
import time
import datetime
import pprint as pp
from datetime import datetime as dt, timedelta as delta
from pyspark.sql.functions import col, udf, explode,lit
from pyspark.sql.types import *
import sys
from pyspark.sql import functions as F
from pyspark.sql.functions import collect_list
from pyspark.sql.window import Window
from pyspark.sql.functions import rank, col
from pyspark.sql.functions import monotonically_increasing_id


# In[3]:


from conf import *


# In[4]:


curr_dt=datetime.datetime.today().strftime('%Y-%m-%d')


# In[5]:


from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
spark = SparkSession.builder.appName('MTA').getOrCreate()
sc = spark.sparkContext
spark = SQLContext(sc)


# In[6]:


query1="""select a.cid,a.eventid,a.impressiondate,a.lineitemid,a.campaignid,a.creativeid,a.load_date,
        b.order_id,b.order_total,b.order_upc_amount
        from """+database_refined+""".attributionv2 AS a
        join """+database_target+""".buyer AS b
        on a.orderids=b.order_id
        where a.cid=b.cid
        and a.campaignid ="""+sys.argv[1]+"""
        and a.load_date > '2019-02-01'
        """
query1
df1=spark.sql(query1)


# In[7]:


query2="""select cid,eventid,impressiondate,lineitemid,campaignid,creativeid,load_date
        from """+database_refined+""".attributioncidmonitorv2
        where campaignid="""+sys.argv[1]+"""
        and load_date > '2019-02-01' """
query2
df2=spark.sql(query2)


# In[8]:


query3="""select campaignId,advertiserId,lineitems._id, lineitems.channelScreentype from """+database_refined+""".campaign
            where _id="""+sys.argv[1]+""""""

df3=spark.sql(query3)


# In[9]:


df6 = df3.withColumn('lineitemid', explode(df3._id))
df7= df3.withColumn('channel_type', explode(df3.channelScreentype))
#.withColumn('channel_type', explode(df3.channelScreentype))

df6 = df6.withColumn("id", monotonically_increasing_id()).drop('campaignId','advertiserId')

df7 = df7.withColumn("id", monotonically_increasing_id())


df8 = df7.join(df6, "id", "outer").drop("id",'_id','channelScreentype')


# In[10]:


file1 =adls_base_path + "/"+ file_path + "/"+"campaign"+sys.argv[2]+"_"+curr_dt+"attrbutionv2.csv"
file2 =adls_base_path + "/"+ file_path + "/"+"campaign"+sys.argv[2]+"_"+curr_dt+"attcidmonitorv2.csv"
file3 =adls_base_path + "/"+ file_path + "/"+"lineitems"+sys.argv[2]+"_"+curr_dt+".csv"


# In[11]:


df1.coalesce(100).write.csv(file1,header="true")
df2.coalesce(500).write.csv(file2,header="true")
df8.coalesce(1).write.csv(file3,header="true")


# In[12]:


df_attribution=spark.read.csv(file1,header=True)
df_attrcidmonitor=spark.read.csv(file2,header=True)
df_lineitems=spark.read.csv(file3,header=True)


# In[13]:


df_attrcidmonitor=df_attrcidmonitor.withColumn('order_id',lit(0))
df_attrcidmonitor=df_attrcidmonitor.withColumn('order_total',lit(0))
df_attrcidmonitor=df_attrcidmonitor.withColumn('order_upc_amount',lit(0))


# In[14]:


df_merged=df_attrcidmonitor.union(df_attribution)


# In[15]:


df_merged=df_merged.orderBy('cid','impressiondate','creativeid')
df_lineitems=df_lineitems.drop ('campaignId')


# In[16]:


df_merged_ln=df_merged.join(df_lineitems,['lineitemid'])


# In[17]:


df_withseg=df_merged_ln.withColumn(
  "Segments",
  F.array(
    F.lit("High Value"),
    F.lit("Covenience"),
    F.lit("Occasional"),
    F.lit("Least Engaged")
  ).getItem(
    (F.rand()*4).cast("int")
  )
)


# In[18]:


df_with_seg_tr=df_withseg.withColumn(
  "TrialRepeat",
  F.array(
    F.lit("trier"),
    F.lit("repeater 1"),
    F.lit("repeater 2"),
    F.lit("repeater 3"),
    F.lit("repeater 4+")
  ).getItem(
    (F.rand()*5).cast("int")
  )
)


# In[33]:


mainReportfile=adls_base_path + "/"+ file_path + "/"+"campaign"+sys.argv[2]+curr_dt+"main_report.csv"
pychattrInputfile=adls_base_path + "/"+ file_path + "/"+"campaign"+sys.argv[2]+curr_dt+"pychattr_input.csv"
bysegmentReportfile=adls_base_path + "/"+ file_path + "/"+"campaign"+sys.argv[2]+curr_dt+"bysegment_report.csv"
bytrierReportfile=adls_base_path + "/"+ file_path + "/"+"campaign"+sys.argv[2]+curr_dt+"bytrier_report.csv"


# In[34]:


df_agg=df_with_seg_tr.groupby('cid','lineitemid','eventid','impressiondate','creativeid','load_date','order_upc_amount','order_id','order_total','advertiserId','Segments','TrialRepeat').agg(F.concat_ws(">", F.collect_list(df_with_seg_tr.channel_type)))
df_agg = df_agg.withColumnRenamed("concat_ws(>, collect_list(channel_type))", "channel_path")


# In[35]:


df_agg3=df_agg.withColumn('c',F.when (F.col("order_id")==0, 0).otherwise(1))    .groupBy('channel_path',"c")    .agg(F.count('c').alias('order_id_c'))    .orderBy("channel_path")


# In[36]:


df_agg4=df_agg3.groupBy("channel_path").pivot('c').agg(F.first('order_id_c')).fillna(0,subset=["0","1"])


# In[37]:


df_agg5=df_agg    .withColumn('c',col('order_upc_amount'))    .groupBy('channel_path')    .agg(F.sum('c').alias('order_upc_amount_c'))    .orderBy("channel_path")


# In[38]:


df_agg6=df_agg4.join(df_agg5,['channel_path'])


# In[47]:


df_agg6=df_agg6.withColumnRenamed("channel_path", "path")  .withColumnRenamed("0", "total_null")    .withColumnRenamed("1", "total_conversions")    .withColumnRenamed("order_upc_amount_c", "total_conversion_value")


# In[48]:


df_agg6.coalesce(1).write.csv(pychattrInputfile,header="true")


# In[41]:


df_agg7=df_agg    .withColumn('c',F.when (F.col("order_id")==0,0))    .groupBy('Segments',"c","channel_path")    .agg(F.count('c').alias('order_id_c'))    .orderBy("Segments")
window = Window.partitionBy(df_agg7['Segments']).orderBy(df_agg7['order_id_c'].desc())
df_aggSegments=df_agg7.select('*', rank().over(window).alias('rank'))    .filter(col('rank') <= 1)
df_aggSegments=df_agg7.drop("c","order_id_c","rank")
df_aggSegments=df_aggSegments.withColumnRenamed("channel_path","Most Common Conversion Path")


# In[42]:


df_aggSegments.coalesce(1).write.csv(bysegmentReportfile,header="true")


# In[43]:


df_agg8=df_agg    .withColumn('c',F.when (F.col("order_id")==0,0))    .groupBy('TrialRepeat',"c","channel_path")    .agg(F.count('c').alias('order_id_c'))    .orderBy("TrialRepeat")

window = Window.partitionBy(df_agg8['TrialRepeat']).orderBy(df_agg8['order_id_c'].desc())
df_aggTR=df_agg8.select('*', rank().over(window).alias('rank'))    .filter(col('rank') <= 5)    .filter('TrialRepeat=="trier"')
df_aggTR=df_aggTR.drop("c","order_id_c","rank")    .withColumnRenamed("channel_path","Most Converted Channel Path")


# In[44]:


df_aggTR.coalesce(1).write.csv(bytrierReportfile,header="true")

