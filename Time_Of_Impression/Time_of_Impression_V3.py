
# coding: utf-8

# In[2]:


#import required packages
import pandas as pd
import numpy as np
import random
import os
import ast
import timeit
import time
import seaborn as sns
#get_ipython().run_line_magic('matplotlib', 'inline')
from functools import reduce
import datetime
import time
import sys
import pprint as pp
from datetime import datetime as dt, timedelta as delta
from pyspark.sql.functions import col, udf, explode,mean,avg,min,max,unix_timestamp,from_unixtime,lit,dayofweek,hour


# In[3]:


from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
spark = SparkSession.builder.appName('Time_Of_Impression').getOrCreate()
sc = spark.sparkContext
spark = SQLContext(sc)


# In[4]:


from conf import *


# In[5]:


curr_dt=datetime.datetime.today().strftime('%Y-%m-%d')


# In[7]:


#Extract data from attributioncidmonitorv2 table.campaignid=122 & 123 in campaign table.
attributioncidmonitorv2="""select 
campaignid
,cid
,lineitemid
,impressiondate
from """+database_refined+""".attributioncidmonitorv2
where campaignid="""+sys.argv[1]+""""""
df_attrcidmonitor=spark.sql(attributioncidmonitorv2)


# In[8]:


#Extract data from attributionv2 table.
attributionv2="""select 
campaignid,
cid,
lineitemid,
impressiondate,
orderids
from """+database_refined+""".attributionv2 
where 
campaignid="""+sys.argv[1]+""""""
df_attr=spark.sql(attributionv2)


# In[9]:


#get the buyer table from target layer first
buyer="""select 
campaign_id,
advertiserid,
retailer_key,
order_id,
upc,
order_timestamp
from """+database_target+""".buyer
where
campaign_id="""+sys.argv[1]+""""""
df_buyer=spark.sql(buyer)


# In[10]:


# Adls output file location 
file1 =adls_base_path + "/"+ file_path + "/"+sys.argv[2]+"_"+curr_dt+"_"+"attributioncidmonitorv2.csv"
file2 =adls_base_path + "/"+ file_path + "/"+sys.argv[2]+"_"+curr_dt+"_"+"attributionv2.csv"
file3 =adls_base_path + "/"+ file_path + "/"+sys.argv[2]+"_"+curr_dt+"_"+"buyer.csv"
First_to_last_impression =adls_base_path + "/"+ file_path + "/"+sys.argv[2]+"_"+curr_dt+"_"+"First_to_last_impression.csv"
First_impression_to_purchase =adls_base_path + "/"+ file_path + "/"+sys.argv[2]+"_"+curr_dt+"_"+"First_impression_to_purchase.csv"
Last_impression_to_purchase =adls_base_path + "/"+ file_path + "/"+sys.argv[2]+"_"+curr_dt+"_"+"Last_impression_to_purchase.csv"
Heatmap =adls_base_path + "/"+ file_path + "/"+sys.argv[2]+"_"+curr_dt+"_"+"Heatmap.csv"


# In[12]:


df_attrcidmonitor.coalesce(1000).write.csv(file1,header="true")
df_attr.coalesce(500).write.csv(file2,header="true")
df_buyer.coalesce(100).write.csv(file3,header="true")


# In[13]:


df_attrcidmonitor= spark.read.csv(file1,header=True)
df_attr=spark.read.csv(file2,header=True)
df_buyer=spark.read.csv(file3,header=True)


# In[18]:


#add orderids column to help union it with attributionv2 table
df_attrcidmonitor=df_attrcidmonitor.withColumn('orderids',lit('null'))


# In[19]:


#Union of both attributioncidmonitorv2 and attributionv2 tables
df_all=df_attrcidmonitor.unionAll(df_attr)


# In[23]:


#Number of IDs with more than 100 touchpoints
df_tp=df_all.groupby('cid').count()
#df_tp.filter(df_tp['count']>100).count()


# In[25]:


#Calculating mininum and maximum impressiondate for each ID
df_time=df_all.groupby(['campaignid','cid']).agg(min("impressiondate").alias('FirstImpression'),max('impressiondate').alias('LastImpression'))
#df_time.show(5)


# In[26]:


timeDiff = (unix_timestamp("LastImpression", "yyyy-MM-dd'T'HH:mm:ss'Z'") - unix_timestamp("FirstImpression", "yyyy-MM-dd'T'HH:mm:ss'Z'"))
df_time = df_time.withColumn("Duration", timeDiff)
#df_time.show(10)


# In[28]:


#Taking only those IDs with a difference of first impression and last impression btw 5 seconds and 30 days
df_time_total=df_time.filter((df_time['Duration']>5) & (df_time['Duration']<3000000))


# In[29]:


df_time_total.write.csv(First_to_last_impression,header="true")


# In[32]:


#grouping by CID and OrderIDs
df4=df_all.groupby(['cid','orderids']).agg(min('impressiondate').alias('MinImpressionDate'))


# In[33]:


#left joining table containing df4 records with buyer table
df4_buyer=df4.join(df_buyer,df4.orderids==df_buyer.order_id,how='left')


# In[35]:


df4_buyer1=df4_buyer.groupby(['cid','campaign_id','advertiserid','retailer_key']).agg(min('MinImpressionDate').alias('FirstImpression'),min('order_timestamp'))


# In[37]:


df4_buyer2=df4_buyer1.filter(df4_buyer1['min(order_timestamp)']!='null')


# In[38]:


#get the duration column
timeDiff = (unix_timestamp("min(order_timestamp)", "yyyy-MM-dd HH:mm:ss") - unix_timestamp("FirstImpression", "yyyy-MM-dd'T'HH:mm:ss'Z'"))
df4_buyer3 = df4_buyer2.withColumn("Duration", timeDiff)
#df4_buyer3.show(10)


# In[39]:


df4_buyer3.write.csv(First_impression_to_purchase,header="true")


# In[41]:


df5=df_attr.groupby(['cid','orderids']).agg(max('impressiondate').alias('MaxImpressionDate'))


# In[42]:


#do an inner join of aggregated attributionv2 and buyer table
df5_buyer=df5.join(df_buyer,df5.orderids==df_buyer.order_id,how='inner')


# In[44]:


df5_buyer1=df5_buyer.groupby(['cid','campaign_id','advertiserid','retailer_key']).agg(max('MaxImpressionDate').alias('LastImpressionDate'),min('order_timestamp'))


# In[45]:


#calculating the duration
timeDiff = (unix_timestamp("min(order_timestamp)", "yyyy-MM-dd HH:mm:ss") - unix_timestamp("LastImpressionDate", "yyyy-MM-dd'T'HH:mm:ss'Z'"))
df5_buyer2 = df5_buyer1.withColumn("Duration", timeDiff)
#df5_buyer2.show(5)


# In[46]:


df5_buyer2.write.csv(Last_impression_to_purchase,header="true")


# In[47]:


df_grid=df5_buyer


# In[48]:


#cid and order_timestamp are mainly needed for this aggregation
df_grid=df_grid.drop('max(impressiondate)','orderids','upc')


# In[51]:


#adding day of week column to the dataframe
df_grid1=df_grid.withColumn("dayofweek",dayofweek(df_grid['order_timestamp']))


# In[52]:


#adding hour of day to the dataframe
df_grid2=df_grid1.withColumn("hour",hour(df_grid1['order_timestamp']))


# In[53]:


df_grid3=df_grid2.groupby(['campaign_id','advertiserid','dayofweek','hour']).count().orderBy(['dayofweek','hour'])


# In[54]:


#drop the row with null values
df_grid4=df_grid3.na.drop()
#df_grid4.show(5)


# In[55]:


df_grid4.write.csv(Heatmap,header="true")

