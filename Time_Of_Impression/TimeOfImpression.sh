
#.  /home/mansari/MTA_Code//conf.py
conf_path=$1
. ${conf_path}

CODE_BASE_PATH=$2

#echo $campaign_id
#echo $campaign_id_ext


set $(echo $campaign_id_ext | sed "s/,/ /g")
for i in $(echo $campaign_id | sed "s/,/ /g");
do


spark-submit --master yarn-client --name test_pyspark --py-files ${conf_path}  --num-executors 20  --executor-cores 10 --executor-memory 20g --driver-memory 15g ${CODE_BASE_PATH}/Time_of_Impression_V3.py "$i" "$1"

shift

done
