
#.  /home/mansari/MTA_Code//conf.py
conf_path=$1
. ${conf_path}

CODE_BASE_PATH=$2

#echo $campaign_id
#echo $campaign_id_ext


set $(echo $campaign_id_ext | sed "s/,/ /g")
for i in $(echo $campaign_id | sed "s/,/ /g");
do


spark-submit --master yarn-client --name test_pyspark --py-files ${conf_path}  --num-executors 20  --executor-cores 10 --executor-memory 20g --driver-memory 15g ${CODE_BASE_PATH}/MTAChannelPathInsight-Final_V3.py "$i" "$1"
#echo "Test"

if [ $? -eq 0 ];
then

spark-submit --master yarn-client --name test_pyspark --py-files ${conf_path}  --num-executors 20  --executor-cores 10 --executor-memory 20g --driver-memory 15g ${CODE_BASE_PATH}/PyChattr_V3.py "$i" "$1"


else 

echo "MTA Code failed with Error"

exit 1;
fi
#echo "$i" "$1"

shift

done
