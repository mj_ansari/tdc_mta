
# coding: utf-8

# In[17]:


import pandas as pd

from pychattr.channel_attribution import PyChAttr
from rpy2.robjects.packages import importr
import rpy2.robjects.packages
import sys
import datetime
import matplotlib.pyplot as plt
import seaborn as sns
# set testthe plot background
sns.set_style("darkgrid")

Rbase = importr("base")
RChannelAttribution = importr("ChannelAttribution")


# In[18]:


from conf import *


# In[19]:


curr_dt=datetime.datetime.today().strftime('%Y-%m-%d')


# In[ ]:


from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
spark = SparkSession.builder.appName('Pychattr').getOrCreate()
sc = spark.sparkContext
spark = SQLContext(sc)


# In[20]:


pychattrInputfile=adls_base_path + "/"+ file_path + "/"+"campaign"+sys.argv[2]+curr_dt+"pychattr_input.csv"
markovReportfile=adls_base_path + "/"+ file_path + "/"+"campaign"+sys.argv[2]+curr_dt+"markov_report.csv"
heuristicReportfile=adls_base_path + "/"+ file_path + "/"+"campaign"+sys.argv[2]+curr_dt+"heuristic_report.csv"


# In[22]:


#input1="adl://dlpreastus2adls1.azuredatalakestore.net/catmktg-edl-prod/sandbox/az-pr-adls-ml1/sbanerj/markov_input1212019-03-10.csv"
#output="adl://dlpreastus2adls1.azuredatalakestore.net/catmktg-edl-prod/sandbox/sbanerj/markov_output.csv"
#input1="demo_data.csv"
df=spark.read.csv(pychattrInputfile,header=True,inferSchema=True)
#pychattrInputfile


# In[23]:


pdf_spark2 = df.toPandas()
#pdf_spark2.head(20)


# In[24]:



pychattr_model = PyChAttr(df=pdf_spark2,
                          markov_model=True,
                          heuristic_model=True,
                          first_touch=True,
                          last_touch=True,
                          linear_touch=True,
                          path_feature="path",
                          conversion_feature="total_conversions",
                          conversion_value_feature="total_conversion_value",
                          null_path_feature="total_null",
                          separator=">",
                          order=1,
                          n_simulations=None,
                          max_step=None,
                          return_transition_probs=True,
                          random_state=26,
                          return_plot_data=True)
pychattr_model.fit()


# In[ ]:


#/hadoopfs/fs2/anaconda3/lib/python3.6/site-packages/rpy2/robjects/pandas2ri.py


# In[25]:


# many of the attributes are just dataframes with the model results
model = pychattr_model.model_
transition_matrix = pychattr_model.transition_matrix_
removal_effects = pychattr_model.removal_effects_

markov = pychattr_model.markov_
heuristic = pychattr_model.heuristic_

conversion_features = pychattr_model.conversion_features_
conversion_value_features = pychattr_model.conversion_value_features_
conv_df = pychattr_model.melted_data_[0]
conv_val_df = pychattr_model.melted_data_[1]


# In[27]:


spark_df1 = spark.createDataFrame(markov.reset_index(drop=False))


# In[28]:


spark_df2 = spark.createDataFrame(heuristic.reset_index(drop=False))


# In[29]:


spark_df1.repartition(1).write.csv(markovReportfile,header="true")


# In[30]:


spark_df2.repartition(1).write.csv(heuristicReportfile,header="true")

